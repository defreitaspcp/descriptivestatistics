#Estatística Descritiva[7]

A estatística descritiva é um ramo da estatística que aplica várias técnicas para descrever e sumarizar um conjunto de dados.

##Média[1]

Média aritmética simples é obtida dividindo-se a soma das observações pelo número delas.

##Moda[2]

Em estatística, moda é uma das medidas de tendência central de um conjunto de dados, assim como a média e a mediana. Ela pode ser definida em moda amostral e populacional. O valor mais frequente.

##Mediana[3]

Mediana é o valor que separa a metade maior e a metade menor de uma amostra, uma população ou uma distribuição de probabilidade.

##Variância[4]

A variância de uma variável aleatória ou processo estocástico é uma medida da sua dispersão estatística, indicando "o quão longe" em geral os seus valores se encontram do valor esperado. A variância amostral é uma medida de dispersão ou variabilidade dos dados, relativamente à medida de localização média. Variância populacional de uma variável de tipo quantitativo, é o valor médio dos quadrados dos desvios relativamente ao valor médio, dos dados que se obtêm quando se observa essa variável sobre todos os elementos da população, que assumimos finita. 

##Coeficiente de variação[5]

O coeficiente de variação(CV), também conhecido como desvio padrão relativo, é uma medida padronizada de dispersão de uma distribuição de probabilidade ou de uma distribuição de frequências.

CV<10% - Baixa Dispersão

10%<CV<20% - Média Dispersão

CV>20% - Alta Dispersão

##Desvio Padrão Populacional e Amostral[6]

Desvio padrão populacional é uma medida de dispersão em torno da média populacional de uma variável aleatória. O termo possui também uma acepção específica no campo da estatística, na qual também é chamado de desvio padrão amostral e indica uma medida de dispersão dos dados em torno de média amostral. Um baixo desvio padrão indica que os pontos dos dados tendem a estar próximos da média ou do valor esperado. Um alto desvio padrão indica que os pontos dos dados estão espalhados por uma ampla gama de valores. O desvio padrão populacional ou amostral é a raiz quadrada da variância populacional ou amostral correspondente, de modo a ser uma medida de dispersão que seja um número não negativo e que use a mesma unidade de medida dos dados fornecidos.

##Referência

[1] Média, wikipedia, https://pt.wikipedia.org/wiki/M%C3%A9dia

[2] Moda, wikipedia, https://pt.wikipedia.org/wiki/Moda

[3] Mediana, wikipedia, https://pt.wikipedia.org/wiki/Mediana_(estat%C3%ADstica)

[4] Variância, wikipedia, https://pt.wikipedia.org/wiki/Vari%C3%A2ncia

[5] Coeficiente de variação, wikipedia, https://pt.wikipedia.org/wiki/Coeficiente_de_varia%C3%A7%C3%A3o

[6] Desvio padrão, wikipedia, https://pt.wikipedia.org/wiki/Desvio_padr%C3%A3o

[7] Estatística Descritiva, wikipedia, https://pt.wikipedia.org/wiki/Estat%C3%ADstica_descritiva