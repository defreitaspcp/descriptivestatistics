import pandas as pd
import numpy as np
import math
from scipy import stats
class QuickSort:
	def partition(self,myList, start, end):
	    pivot = myList[start]
	    left = start+1
	    # Start outside the area to be partitioned
	    right = end
	    done = False
	    while not done:
	        while left <= right and myList[left] <= pivot:
	            left = left + 1
	        while myList[right] >= pivot and right >=left:
	            right = right -1
	        if right < left:
	            done= True
	        else:
	            # swap places
	            temp=myList[left]
	            myList[left]=myList[right]
	            myList[right]=temp
	    # swap start with myList[right]
	    temp=myList[start]
	    myList[start]=myList[right]
	    myList[right]=temp
	    return right


	def quicksort(self,myList, start, end):
	    if start < end:
	        # partition the list
	        split = self.partition(myList, start, end)
	        # sort both halves
	        self.quicksort(myList, start, split-1)
	        self.quicksort(myList, split+1, end)
	    return myList
		

class DescriptiveStatistics:
	def Average(self,data):
		sum = 0
		for i in data:
			sum += i
		return sum / len(data)

	def totalSumSquares(self,data):
		sum = 0
		for i in data:
			sum += math.pow(i - self.Average(data),2)
		return sum

	def max(self,data):
		obj = QuickSort()
		data = obj.quicksort(data,0,len(data)-1)
		return data[len(data)-1]

	def min(self,data):
		obj = QuickSort()
		data = obj.quicksort(data,0,len(data)-1)
		return data[0]

	def median(sel,data):
		obj = QuickSort()
		length = len(data)
		var = obj.quicksort(data,0,length-1)
		if length % 2 == 0:
			return (var[(int(length/2)-1)]+var[int(length/2)])/2.0
		else:
			return var[int(length/2)]

	def medianLow(self,data):
		obj = QuickSort()
		length = len(data)
		var = obj.quicksort(data,0,length-1)
		if length % 2 == 0:
			return (var[(int(length/4)-1)]+var[int(length/4)])/2.0
		else:
			return var[int(length/4)]

	def medianHigh(self,data):
		obj = QuickSort()
		length = len(data)
		var = obj.quicksort(data,0,length-1)
		if length % 2 == 0:
			return (var[(int(length*3/4)-1)]+var[int(length*3/4)])/2.0
		else:
			return var[int(length*3/4)]

	def check(self,data,value):
		count = 0
		for i in data:
			if value == i:
				count+=1
		return count

	def mode(self,data):
		number = 0
		value = 0
		repetition = 0
		for i in data:
			number = i
			if repetition <= self.check(data,number):
				repetition = self.check(data,number)
				value = i
		return value

	def SampleVariance(self,data):
		return self.totalSumSquares(data)/(len(data)-1)

	def PopulationVariance(self,data):
		return self.totalSumSquares(data)/len(data)

	def SampleStandardDeviation(self,data):
		return math.sqrt(self.SampleVariance(data))

	def PopulationStandardDeviation(self,data):
		return math.sqrt(self.PopulationVariance(data))

	def SampleCoefficientVariation(self,data):
		return self.SampleStandardDeviation(data)/self.Average(data)

	def PopulationCoefficientVariation(self,data):
		return self.PopulationStandardDeviation(data)/self.Average(data)

	def SampleCovariance(self,x,y):
		return (self.totalSumSquares(x)*self.totalSumSquares(y))/(len(x)-1)

	def PopulationCovariance(self, x, y):
		return (self.totalSumSquares(x)*self.totalSumSquares(y))/len(x)

if __name__ == "__main__":

    file =  pd.read_csv('AM20181.csv')
    
    CDP = file.CDP
    CDE = file.CDE

    CDP = np.array(CDP)
    CDE = np.array(CDE)

    var = DescriptiveStatistics()

    print("Descriptive Statistical Analysis\n")

    print("Mean is : %f"%var.Average(CDP))
    print("Total sum of Squares of data is : %f"%var.totalSumSquares(CDP))
    print("Sample variance of data is :%f"%var.SampleVariance(CDP))
    print("Population variance of data is : %f"%var.PopulationVariance(CDP))
    print("Sample standard deviation of data is : %f"%var.SampleStandardDeviation(CDP))
    print("Population standard deviation of data is : %f"%var.PopulationStandardDeviation(CDP))
    print("Sample Coefficient of Variation :%f"%var.SampleCoefficientVariation(CDP))
    print("Population Coefficient of Variation of data is :%f"%var.PopulationCoefficientVariation(CDP))
    print("Maximum is : %f"%var.max(CDP))
    print("Minimum is : %f"%var.min(CDP))
    print("Median (middle value) of data is : %f"%var.median(CDP))
    print("Low median of data is : %f"%var.medianLow(CDP))
    print("High median of data is : %f:"%var.medianHigh(CDP))
    print("Mode (most common value) of discrete data is :%f"%var.mode(CDP))
    print("\n")
    print("Mean is : %f"%var.Average(CDE))
    print("Total sum of Squares of data is : %f"%var.totalSumSquares(CDE))
    print("Sample variance of data is :%f"%var.SampleVariance(CDE))
    print("Population variance of data is : %f"%var.PopulationVariance(CDE))
    print("Sample standard deviation of data is : %f"%var.SampleStandardDeviation(CDE))
    print("Population standard deviation of data is : %f"%var.PopulationStandardDeviation(CDE))
    print("Sample Coefficient of Variation :%f"%var.SampleCoefficientVariation(CDE))
    print("Population Coefficient of Variation of data is :%f"%var.PopulationCoefficientVariation(CDE))
    print("Maximum is : %f"%var.max(CDE))
    print("Minimum is : %f"%var.min(CDE))
    print("Median (middle value) of data is : %f"%var.median(CDE))
    print("Low median of data is : %f"%var.medianLow(CDE))
    print("High median of data is : %f:"%var.medianHigh(CDE))
    print("Mode (most common value) of discrete data is :%f"%var.mode(CDE))
    print("\n")
    print("Sample Covariance is : %f"%var.SampleCovariance(CDP,CDE))
    print("Population Covariance is : %f"%var.PopulationCovariance(CDP,CDE))

'''
Descriptive Statistics
Author: de Freitas, P.C.P
References
[1] Data Structures Algorithms - Quicksort, pythonschool, https://pythonschool.net/data-structures-algorithms/quicksort/
[2] Mean, wikipedia, https://en.wikipedia.org/wiki/Mean
[3] Total sum of square, wikipedia, https://en.wikipedia.org/wiki/Total_sum_of_squares
[4] Variance, wikipedia, https://en.wikipedia.org/wiki/Variance
[5] Standard deviation, wikipedia, https://en.wikipedia.org/wiki/Standard_deviation
[6] Coefficient of variation, wikipedia, https://en.wikipedia.org/wiki/Coefficient_of_variation
[7] Median, wikipedia, https://en.wikipedia.org/wiki/Median
[8] Mode, wikipedia, https://en.wikipedia.org/wiki/Mode_(statistics)
[9] Covariance, wikipedia, https://en.wikipedia.org/wiki/Covariance
'''